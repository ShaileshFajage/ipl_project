
const iplData = require("./json")

const matches = iplData.matches;

const deliveries = iplData.deliveries;

const matchesWon = matches.reduce((matchesWon, eachData) =>{


    if(matchesWon[eachData.winner])
    {
        if(matchesWon[eachData.winner][eachData.season])
        {
            matchesWon[eachData.winner][eachData.season]+= 1
        }
        else
        {
            matchesWon[eachData.winner][eachData.season] = 1
        }
    }
    else
    {
        matchesWon[eachData.winner] = {};
        matchesWon[eachData.winner][eachData.season] = 1
    }

    return matchesWon;

}, {})

console.log(matchesWon);
