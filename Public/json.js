

let csvToJson = require("convert-csv-to-json");

let fileMatches = "/home/shailesh/Desktop/IPLProject/Public/Data/matches.csv"
let fileDeliveries = "/home/shailesh/Desktop/IPLProject/Public/Data/deliveries.csv"

let matches = csvToJson.fieldDelimiter(",").getJsonFromCsv(fileMatches);

let deliveries = csvToJson.fieldDelimiter(",").getJsonFromCsv(fileDeliveries);


module.exports = {
    matches,
    deliveries
}
