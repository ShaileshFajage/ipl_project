

const iplData = require("./json")

const matches = iplData.matches;

const deliveries = iplData.deliveries;


const match2017 = matches.filter((val) => {
    if(val.season==2017)
    {
        return val;
    }
})


let firstId = Number(match2017[0].id);
let lastId = Number(match2017[match2017.length-1].id);


const filteredDeleveries = deliveries.filter((val) =>{

    let num = Number(val.match_id);

    if(num>=firstId && num<=lastId)
    {

        return val;
    }
} )



let result1 = filteredDeleveries.reduce((result, each) => {

    if(result.hasOwnProperty(each.bowler))
    {

        result[each.bowler] += Number(each.total_runs);
        
    }
    else
    {

        result[each.bowler] = Number(each.total_runs);

    }

    return result;

}, {})

//console.log(result1)

let result2 = filteredDeleveries.reduce((result, each) => {

    if(result.hasOwnProperty(each.bowler))
    {
        if(Number(each.ball)==6)
        {
            result[each.bowler] += 1;
        }
        
    }
    else
    {

        result[each.bowler] = 0;

    }

    return result;

}, {})

//console.log(result2);

for(let property in result1)
{
    result1[property] = result1[property]/result2[property];

}

//console.log(result1)

const sortable = Object.fromEntries(
    Object.entries(result1).sort(([,a],[,b]) => a-b)
);

const finalResult = Object.keys(sortable).slice(0, 10).reduce((result, key) => {
    result[key] = sortable[key];

    return result;
}, {});

console.log(finalResult)


