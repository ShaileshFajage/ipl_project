

const iplData = require("./json")

const matches = iplData.matches;

const deliveries = iplData.deliveries;


const match2016 = matches.filter((val) => {
    if(val.season==2016)
    {
        return val;
    }
})


let firstId = Number(match2016[0].id);
let lastId = Number(match2016[match2016.length-1].id);

const filteredDeleveries = deliveries.filter((val) =>{

    let num = Number(val.match_id);

    if(num>=firstId && num<=lastId)
    {

        return val;
    }
} )



let result = filteredDeleveries.reduce((result, each) => {

    if(result.hasOwnProperty(each.bowling_team))
    {
        result[each.bowling_team]+= Number(each.extra_runs);
    }
    else
    {
        result[each.bowling_team] = Number(each.extra_runs);
    }

    return result;

}, {})

console.log(result);





